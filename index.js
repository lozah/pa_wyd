import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuardService as AuthGuard} from '../core/auth/guards/auth-guard.service';
import {ProjectComponent} from './pages/project/project.component';
import {ProjectIdeComponent} from './pages/project-ide/project-ide.component';
import {ProjectDetailsComponent} from './pages/project-details/project-details.component';
import {ProjectMembersComponent} from './pages/project-members/project-members.component';
import {ProjectAdministrationComponent} from './pages/project-administration/project-administration.component';
import {ProjectOrganizationComponent} from './pages/project-organization/project-organization.component';

const routes: Routes = [
    {
        path: 'projects',
        redirectTo: 'dashboard/projects',
        pathMatch: 'full'
    },
    {
        path: 'projects/:project/ide',
        component: ProjectComponent,
        children: [
            {
                path: '',
                component: ProjectIdeComponent,
                canActivate: [AuthGuard]
            },
            {
                path: ':user/public',
                component: ProjectIdeComponent,
                canActivate: [AuthGuard]
            }
        ]
    },
    {
        path: 'projects/:project/administration',
        component: ProjectAdministrationComponent,
        children: [
            {
                path: '',
                redirectTo: 'details',
                pathMatch: 'full'
            },
            {
                path: 'details',
                component: ProjectDetailsComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'members',
                component: ProjectMembersComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'organization',
                component: ProjectOrganizationComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProjectRoutingModule {
}
